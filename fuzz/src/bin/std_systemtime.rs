#![no_main]

#[derive(Debug, arbitrary::Arbitrary)]
struct Config {
	rate: std::time::Duration,
	capcaity: u32,
}

impl Config {
	fn into_config(&self) -> Option<rl_core::Config> {
		if self.rate.is_zero() { return None }
		Some(rl_core::Config::new(self.rate, self.capcaity))
	}
}

#[derive(Debug, arbitrary::Arbitrary)]
enum Event {
	Acquire(u32),
	AcquireRange(std::ops::RangeInclusive<u32>),
	AcquireUpTo(u32),
	AddCapacity(u32),
	AddLimitedCapacity(u32),
	Capacity,
	ChangeConfig(Config),
	ForceAcquire(u32),
	Reverse(std::time::Duration),
	Simplify,
	Wait(std::time::Duration),
}

#[derive(Debug, arbitrary::Arbitrary)]
struct Job {
	cfg: Config,
	events: Vec<Event>,
}

fn run(data: Job) -> Option<()> {
	let mut cfg = data.cfg.into_config()?;
	let mut now = std::time::UNIX_EPOCH;
	let mut tracker = rl_core::Tracker::new_at(now);
	for event in data.events {
		match event {
			Event::Acquire(count) => {
				let _ = tracker.acquire_at(&cfg, count, now);
			}
			Event::AcquireRange(range) => {
				let _ = tracker.acquire_range_at(&cfg, range, now);
			}
			Event::AcquireUpTo(count) => {
				let _ = tracker.acquire_up_to_at_2(&cfg, count, now);
			}
			Event::AddCapacity(count) => {
				tracker.add_capacity_at(&cfg, count, now);
			}
			Event::AddLimitedCapacity(count) => {
				tracker.add_limited_capacity_at(&cfg, count, now);
			}
			Event::Capacity => {
				tracker.capacity_at(&cfg, now);
			}
			Event::ChangeConfig(config) => {
				cfg = config.into_config()?;
			}
			Event::ForceAcquire(count) => {
				tracker.force_acquire_at(&cfg, count, now);
			}
			Event::Reverse(duration) => {
				now = now.checked_sub(duration)?;
			}
			Event::Simplify => {
				tracker.simplify_at(&cfg, now);
			}
			Event::Wait(duration) => {
				now = now.checked_add(duration)?;
			}
		}
	}

	Some(())
}

libfuzzer_sys::fuzz_target!(|data: Job| {
	let _ = run(data);
});
