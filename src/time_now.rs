pub trait TimeNow: crate::Time {
	fn now() -> Self;
}
