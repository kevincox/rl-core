#[test]
fn test_serde_serialize() -> Result<(), Box<dyn std::error::Error>> {
	let mut now = std::time::UNIX_EPOCH;
	let cfg = crate::Config::new(std::time::Duration::from_secs(1), 10);
	let mut t = crate::Tracker::default();

	assert_eq!(
		serde_json::to_string(&t)?,
		r#"{}"#);

	now += std::time::Duration::from_secs(20);
	t.acquire_at(&cfg, 4, now)?;

	assert_eq!(
		serde_json::to_string(&t)?,
		r#"{"s":[14,0]}"#);

	assert_eq!(
		serde_json::to_string(&crate::Tracker::<std::time::SystemTime>::overfull(100))?,
		r#"{"o":100}"#);

	Ok(())
}

#[test]
fn test_serde_deserialize() -> Result<(), Box<dyn std::error::Error>> {
	assert_eq!(
		serde_json::from_str::<crate::Tracker<std::time::SystemTime>>(r#"{}"#)?,
		crate::Tracker::full());

	assert_eq!(
		serde_json::from_str::<crate::Tracker<std::time::SystemTime>>(r#"{"s":[14,0]}"#)?,
		crate::Tracker::empty_at(std::time::UNIX_EPOCH + std::time::Duration::from_secs(14)));

	assert_eq!(
		serde_json::from_str::<crate::Tracker<std::time::SystemTime>>(r#"{"o":100}"#)?,
		crate::Tracker::overfull(100));

	// Currently a time parameter overrides an overfull parameter.
	// Hopefully no one depends on this but we have a test to ensure that any change to this is intentional.
	assert_eq!(
		serde_json::from_str::<crate::Tracker<std::time::SystemTime>>(r#"{"s": [4, 400000000], "o": 100}"#)?,
		crate::Tracker::empty_at(std::time::UNIX_EPOCH + std::time::Duration::from_millis(4_400)));

	Ok(())
}

#[test]
fn test_serde_old() -> Result<(), Box<dyn std::error::Error>> {
	assert_eq!(
		serde_json::from_str::<crate::Tracker<std::time::SystemTime>>(r#"{"l":null}"#)?,
		crate::Tracker::full());

	Ok(())
}
