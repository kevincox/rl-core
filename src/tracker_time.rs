impl<T: crate::TimeNow> crate::Tracker<T> {
	/// Create a tracker that starts empty at the current time.
	///
	/// Equivalent to [`Tracker::empty_at(T::now())`][Tracker::empty_at()].
	pub fn empty() -> Self {
		Self::empty_at(T::now())
	}

	/// Create a tracker that has the specified capacity.
	///
	/// Equivalent to [`Tracker::with_capacity_at(config, capacity, T::now())`](Tracker::with_capacity_at()).
	pub fn with_capacity(
		config: &crate::Config<T>,
		capacity: u32,
	) -> Self {
		Self::with_capacity_at(config, capacity, T::now())
	}

	/// Create a tracker that has the specified capacity without overfilling.
	///
	/// Equivalent to [`Tracker::with_limited_capacity_at(config, capacity, T::now())`](Tracker::with_limited_capacity_at()).
	pub fn with_limited_capacity(
		config: &crate::Config<T>,
		capacity: u32,
	) -> Self {
		Self::with_limited_capacity_at(config, capacity, T::now())
	}

	/// Returns the tokens currently available.
	///
	/// Equivalent to [`Tracker::capacity_at(T::now())`](Tracker::capacity_at).
	pub fn capacity(&self,
		config: &crate::Config<T>,
	) -> u32 {
		self.capacity_at(config, T::now())
	}

	/// Attempt to acquire `count` tokens from the rate limiter at the current time
	///
	/// Equivalent to [`Tracker::acquire_at(config, count, T::now())`](Tracker::acquire_at()).
	pub fn acquire(&mut self,
		config: &crate::Config<T>,
		count: u32,
	) -> Result<(), crate::Denied<T>> {
		self.acquire_at(config, count, T::now())
	}

	/// Acquire tokens from the tracker.
	///
	/// Equivalent to [`Tracker::acquire_range_at(config, request, T::now())`](Tracker::acquire_range_at()).
	pub fn acquire_range(&mut self,
		config: &crate::Config<T>,
		request: std::ops::RangeInclusive<u32>,
	) -> Result<u32, crate::Denied<T>> {
		self.acquire_range_at(config, request, T::now())
	}

	/// Acquire up to `count` tokens from the rate limiter at the current time.
	///
	/// Equivalent to [`Tracker::acquire_up_to_at(config, count, T::now())`](Tracker::acquire_up_to_at()).
	#[deprecated(note="Use acquire_up_to_2.")]
	pub fn acquire_up_to(&mut self,
		config: &crate::Config<T>,
		count: u32,
	) -> Result<u32, crate::TooEarly<T>> {
		#[allow(deprecated)]
		self.acquire_up_to_at(config, count, T::now())
	}

	/// Acquire up to `count` tokens from the rate limiter at the current time.
	///
	/// Equivalent to [`Tracker::acquire_up_to_at_2(config, count, T::now())`](Tracker::acquire_up_to_at_2()).
	pub fn acquire_up_to_2(&mut self,
		config: &crate::Config<T>,
		count: u32,
	) -> Result<u32, crate::Denied<T>> {
		self.acquire_up_to_at_2(config, count, T::now())
	}

	/// Force acquire tokens at the current time.
	///
	/// Equivalent to [`Tracker::force_acquire_at(config, count, T::now())`](Tracker::force_acquire_at()).
	pub fn force_acquire(
		&mut self,
		config: &crate::Config<T>,
		count: u32,
	) {
		self.force_acquire_at(config, count, T::now())
	}

	/// Add capacity.
	///
	/// Equivalent to [`Tracker::add_capacity_at(config, count, tokens, T::now())`](Tracker::add_capacity_at()).
	pub fn add_capacity(
		&mut self,
		config: &crate::Config<T>,
		tokens: u32,
	) {
		self.add_capacity_at(config, tokens, T::now())
	}

	/// Add capacity without overfilling.
	///
	/// Equivalent to [`Tracker::add_limited_capacity_at(config, count, tokens, T::now())`](Tracker::add_limited_capacity_at()).
	pub fn add_limited_capacity(
		&mut self,
		config: &crate::Config<T>,
		tokens: u32,
	) {
		self.add_limited_capacity_at(config, tokens, T::now())
	}

	/// Attempts to minimize the state at the current time.
	///
	/// Equivalent to [`Tracker::simplify_at(config, T::now())`](Tracker::simplify_at()).
	pub fn simplify(&mut self,
		config: &crate::Config<T>,
	) -> bool {
		self.simplify_at(config, T::now())
	}
}

impl<T: crate::Time> Default for crate::Tracker<T> {
	/// Create a full tracker.
	///
	/// See [`Tracker::full()`] for more details.
	fn default() -> Self {
		Self::full()
	}
}
