use crate::Duration as _;

/// Config for rate limiting.
///
/// This object contains the configuration for the rate limits but does not actually hold any rate limiting state. It is often constructed once globally and used for many rate limits however it can also be constructed dynamically if desired.
#[derive(Clone,Debug)]
pub struct Config<T: crate::Time = std::time::SystemTime> {
	pub(crate) rate: T::Duration,
	pub(crate) burst: u32,
}

impl<T: crate::Time> Config<T> {
	/// Create a new config.
	///
	/// - `rate`: The amount of time that must elapse between each unit of request on average. Must be greater than zero.
	/// - `burst`: The maximum units that can be used instantaneously.
	///
	/// In other words this is configuring a token bucket where `rate` is the fill rate and `burst` is the maximum capacity.
	///
	/// The tokens are always replenished one-at-a-time. For example "10 tokens even 1min" isn't supported. Instead you would configure "1 token every 6 seconds" which is the same average rate but granted evenly throughout the minute.
	pub const fn new(rate: T::Duration, burst: u32) -> Self {
		Config{rate, burst}
	}

	/// Create a config that will never generate tokens.
	///
	/// Acquisitions will always fail unless the tracker is overfull.
	pub fn disabled() -> Self {
		Config{
			rate: T::Duration::null(),
			burst: 0,
		}
	}

	/// Returns true if this config will not generate tokens on its own.
	///
	/// Note that a disabled config can still grant tokens if the tracker is overfull.
	pub fn is_disabled(&self) -> bool {
		self.burst == 0
	}

	/// The rate at which new tokens appear in the bucket.
	///
	/// This duration represents the period between token additions to the capacity tokens always become available one at a time.
	pub fn token_rate(&self) -> T::Duration {
		self.rate.clone()
	}

	/// The maximum fill capacity.
	///
	/// This is the maximum capacity that trackers will fill to if left idle.
	///
	/// Note that the capacity may exceed this value if artificially raised such as via [`Tracker::overfull()`], [`Tracker::with_capacity_at()`] or [`Tracker::add_capacity_at()`].
	pub fn burst_capacity(&self) -> u32 {
		self.burst
	}

	/// The time it takes to completely fill the tracker.
	pub(crate) fn fill_time(&self) -> T::Duration {
		self.rate.clone().mul(self.burst)
	}
}
