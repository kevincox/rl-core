impl crate::Time for std::time::SystemTime {
	type Duration = std::time::Duration;

	fn add(self, duration: Self::Duration) -> Self {
		self.checked_add(duration)
			.unwrap_or(
				// TODO: Get real max value.
				self.max(
					std::time::SystemTime::UNIX_EPOCH
					+ std::time::Duration::from_secs(10_000 * 365 * 24 * 3600)))
	}

	fn sub(self, duration: Self::Duration) -> Self {
		self.checked_sub(duration)
			// This probably isn't the minimum representable system time, but Rust doesn't provide this to us. It is probably far enough back for any reasonable use case.
			.unwrap_or(std::time::SystemTime::UNIX_EPOCH)
	}

	fn since(self, earlier: Self) -> Option<Self::Duration> {
		self.duration_since(earlier).ok()
	}
}

impl crate::TimeNow for std::time::SystemTime {
	fn now() -> Self {
		std::time::SystemTime::now()
	}
}
