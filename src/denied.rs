/// A reason for denial.
#[derive(Debug,PartialEq)]
#[non_exhaustive]
pub enum Denied<T = std::time::SystemTime> {
	/// The maximum request was less than the minimum.
	EmptyRequest,
	/// The request can not be made at this time due to rate limiting rules.
	TooEarly(crate::TooEarly<T>),
	/// The request is larger than the max bucket size and will never be allowed with the current config.
	///
	/// Note: [Overfilling](Tracker::overfull()) can allow requests larger than buckets, however this never happens by waiting. It must be done explicitly by the application. Overfilling can allow arbitrarily large requests to succeed on any config.
	TooBig,
}

impl<T: std::fmt::Debug> std::fmt::Display for Denied<T> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		match self {
			Denied::EmptyRequest => f.write_str("Request maximum is less than minimum."),
			Denied::TooEarly(err) => err.fmt(f),
			Denied::TooBig => f.write_str("Request cost is larger than bucket size."),
		}
	}
}

impl<T: std::fmt::Debug> std::error::Error for Denied<T> {}

impl<T> From<crate::TooEarly<T>> for Denied<T> {
	fn from(e: crate::TooEarly<T>) -> Self {
		Denied::TooEarly(e)
	}
}
