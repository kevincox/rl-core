macro_rules! test {
	($name:ident($now:pat_param) $body:block) => {
		#[test]
		fn $name() {
			fn f<
				T: crate::TimeNow<Duration=std::time::Duration>
					+ Copy
					+ std::ops::Add<std::time::Duration, Output=T>
					+ std::ops::AddAssign<std::time::Duration>
			>($now: T)
				$body

			f(std::time::UNIX_EPOCH);
			f(std::time::Instant::now());

			#[cfg(feature="tokio")] f(tokio::time::Instant::now());
		}
	};
}

test!(test_time_now(now) {
	let cfg = crate::Config::new(std::time::Duration::from_secs(1), 10);

	let mut t = crate::Tracker::full();

	assert_eq!(t.capacity_at(&cfg, now), 10);
	assert_eq!(t.capacity(&cfg), 10);
	assert_eq!(t.acquire(&cfg, 1), Ok(()));
});
