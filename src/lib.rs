//! Simple, pure rate limiting.
//!
//! ```
//! const CONFIG: rl_core::Config = rl_core::Config::new(
//! 	std::time::Duration::from_secs(1),
//! 	3,
//! );
//! let mut tracker = rl_core::Tracker::full();
//! assert!(tracker.acquire(&CONFIG, 1).is_ok());
//! assert!(tracker.acquire(&CONFIG, 2).is_ok());
//! assert!(tracker.acquire(&CONFIG, 1).is_err());
//! ```

mod config; pub use config::*;
mod denied; pub use denied::*;
mod too_early; pub use too_early::*;
mod tracker; pub use tracker::*;
mod tracker_time;

// These traits are currently blocked from external impls.
mod duration; pub(crate) use duration::*;
mod time; pub(crate) use time::*;
mod time_now; pub(crate) use time_now::*;

mod std_duration;
mod std_instant;
mod std_systemtime;

#[cfg(feature="serde")] mod serde;
#[cfg(feature="serde")] mod time_serde;
#[cfg(feature="serde")] pub(crate) use time_serde::*;

#[cfg(feature="tokio")] mod tokio_instant;

#[cfg(feature="serde")] #[cfg(test)] mod serde_test;

#[cfg(test)] mod tracker_test;
#[cfg(test)] mod tracker_time_test;
