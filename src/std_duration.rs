impl crate::Duration for std::time::Duration {
	fn mul(self, s: u32) -> Self {
		self.checked_mul(s).unwrap_or(std::time::Duration::MAX)
	}

	fn div(self, that: Self) -> u32 {
		(self.as_nanos() / that.as_nanos())
			.try_into()
			.unwrap_or(u32::MAX)
	}

	fn null() -> Self {
		std::time::Duration::from_nanos(1)
	}

	fn zero() -> Self {
		std::time::Duration::ZERO
	}
}
