impl crate::Time for tokio::time::Instant {
	type Duration = std::time::Duration;

	// Tokio's Instant is just a wrapper for `std::time::Instant` that can be paused and resumed for testing. So we will just unwrap and defer to the implementation on `std::time::Instant`.

	fn add(self, duration: Self::Duration) -> Self {
		self.into_std().add(duration).into()
	}

	fn sub(self, duration: Self::Duration) -> Self {
		self.into_std().sub(duration).into()
	}

	fn since(self, earlier: Self) -> Option<Self::Duration> {
		self.into_std().checked_duration_since(earlier.into_std())
	}
}

impl crate::TimeNow for tokio::time::Instant {
	fn now() -> Self {
		tokio::time::Instant::now()
	}
}
