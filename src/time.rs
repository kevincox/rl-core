pub trait Time: Clone + std::fmt::Debug + Ord {
	type Duration: crate::Duration;

	/// Saturating addition.
	fn add(self, duration: Self::Duration) -> Self;

	fn add_assign(&mut self, duration: Self::Duration) {
		*self = self.clone().add(duration);
	}

	/// Saturating subtraction.
	fn sub(self, duration: Self::Duration) -> Self;

	/// Saturating in-place subtraction.
	fn sub_assign(&mut self, duration: Self::Duration) {
		*self = self.clone().sub(duration);
	}

	fn since(self, earlier: Self) -> Option<Self::Duration>;
}
