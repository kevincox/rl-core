pub trait Duration: Clone + std::fmt::Debug + Ord {
	fn mul(self, s: u32) -> Self;

	/// Divide
	///
	/// The right hand side is never zero.
	fn div(self, that: Self) -> u32;

	/// A value that is used to mock out math for disabled trackers.
	///
	/// Must not be zero or divisions by zero will occur.
	fn null() -> Self;

	fn zero() -> Self;
}
