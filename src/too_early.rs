#[derive(Debug,PartialEq)]
pub struct TooEarly<T = std::time::SystemTime> {
	pub(crate) next: T,
}

impl<T: Clone> TooEarly<T> {
	/// Return when the request will be allowed.
	///
	/// Returns the earliest time that the request can possibly succeed. If there are no other requests in the interim than retrying the same request will succeed at that time.
	pub fn available_at(&self) -> T {
		self.next.clone()
	}
}

impl<T: std::fmt::Debug> std::fmt::Display for TooEarly<T> {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "Try again after {:?}", self.next)
	}
}

impl<T: std::fmt::Debug> std::error::Error for TooEarly<T> {}
