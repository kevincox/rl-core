impl crate::Time for std::time::Instant {
	type Duration = std::time::Duration;

	fn add(mut self, mut duration: Self::Duration) -> Self {
		if let Some(r) = self.checked_add(duration) {
			return r
		}

		// This is pretty hacky, we don't have a clean way to get the minimum so we just do a limited binary search. Hope this path is never hit.
		// https://internals.rust-lang.org/t/instant-systemtime-min-max/21375
		for _ in 0..10 {
			duration /= 2;
			if let Some(sum) = self.checked_add(duration) {
				self = sum;
			}
		}
		self
	}

	fn sub(mut self, mut duration: Self::Duration) -> Self {
		if let Some(r) = self.checked_sub(duration) {
			return r
		}

		// This is pretty hacky, we don't have a clean way to get the minimum so we just do a limited binary search. Hope this path is never hit.
		// https://internals.rust-lang.org/t/instant-systemtime-min-max/21375
		for _ in 0..10 {
			duration /= 2;
			if let Some(rem) = self.checked_sub(duration) {
				self = rem;
			}
		}
		self
	}

	fn since(self, earlier: Self) -> Option<Self::Duration> {
		self.checked_duration_since(earlier)
	}
}

impl crate::TimeNow for std::time::Instant {
	fn now() -> Self {
		std::time::Instant::now()
	}
}
