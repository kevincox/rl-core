pub(crate) trait TimeSerde: crate::Time {
	type Ser<'a>: serde::Serialize where Self: 'a;
	type De<'a>: serde::Deserialize<'a>;

	fn serialize<'a>(&'a self) -> Self::Ser<'a>;
	fn deserialize(this: Self::De<'_>) -> Self;
}

impl TimeSerde for std::time::SystemTime {
	type Ser<'a> = (u64, u32);
	type De<'a> = Self::Ser<'a>;

	fn serialize<'a>(&'a self) -> Self::Ser<'a> {
		let duration = self.duration_since(std::time::UNIX_EPOCH).unwrap();
		(duration.as_secs(), duration.subsec_nanos())
	}

	fn deserialize((s, ns): Self::De<'_>) -> Self {
		std::time::UNIX_EPOCH + std::time::Duration::new(s, ns)
	}
}
